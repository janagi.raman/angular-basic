import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { DeptComponent } from './dept/dept.component';
import { LibComponent } from './lib/lib.component';
import { SportsComponent } from './sports/sports.component';
import { ITComponent } from './dept/it/it.component';
import { CIVILComponent } from './dept/civil/civil.component';
import { EEEComponent } from './dept/eee/eee.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    ContentComponent,
    DeptComponent,
    LibComponent,
    SportsComponent,
    ITComponent,
    CIVILComponent,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
    EEEComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule                                                                                                                                                                                                                                                                               
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
