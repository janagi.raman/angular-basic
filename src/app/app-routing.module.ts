import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CIVILComponent } from './dept/civil/civil.component';
import { DeptComponent } from './dept/dept.component';
import { EEEComponent } from './dept/eee/eee.component';
import { CSEComponent } from './dept/cse/cse.component';
import { ITComponent } from './dept/it/it.component';
import { LibComponent } from './lib/lib.component';
import { SportsComponent } from './sports/sports.component';

const routes: Routes = [
  {path:"dept",component:DeptComponent,children:[
    {path:"cse",component:CSEComponent},
    {path:"it",component:ITComponent},
    {path:"eee",component:EEEComponent},
    {path:"civil",component:CIVILComponent}
  ]},
  {path:"lib",component:LibComponent},
  {path:"sports",component:SportsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
